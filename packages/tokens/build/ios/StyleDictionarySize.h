
// StyleDictionarySize.h
//

// Do not edit directly
// Generated on Wed, 15 Mar 2023 08:05:14 GMT


#import <Foundation/Foundation.h>


extern float const SizeFontSmall;
extern float const SizeFontMedium;
extern float const SizeFontLarge;
extern float const SizeFontBase;
