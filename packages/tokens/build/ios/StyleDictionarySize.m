
//
// StyleDictionarySize.m
//

// Do not edit directly
// Generated on Wed, 15 Mar 2023 08:05:14 GMT


#import "StyleDictionarySize.h"


float const SizeFontSmall = 12.00f;
float const SizeFontMedium = 16.00f;
float const SizeFontLarge = 32.00f;
float const SizeFontBase = 16.00f;
